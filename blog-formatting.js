/**
 * Created by wesm on 12/9/16.
 */


function fixBlogPosition() {
    navheight = $('#top-nav').css('height');
    $('#blog-main').css('margin-top',navheight);
}

fixBlogPosition();

$(window).resize(function () {
    fixBlogPosition();
});